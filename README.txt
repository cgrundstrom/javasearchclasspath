JavaSearchClassPath

Searches the Java CLASSPATH for a class. Useful for discovering which jar file contains a specified java class.

To use, add the bin directory to your PATH, set your classpath using the "setcp" script, then run javasearchclasspath.

Example:
  PATH=$HOME/projects/javasearchclasspath/bin:$PATH
  export CLASSPATH=
  cd $HOME/projects
  . setcp
  javasearchclasspath org.javasearchclasspath.JavaSearchClassPath

It may be convenient to add an alias for running javasearchclasspath

  alias jscp=javasearchclasspath

JavaSearchClassPath works under Linux and MacOS. You can also use it on Windows if you are using cygwin. It has
not been ported to Windows and the CMD shell.

JavaSearchClassPath is licensed using Apache License V2.0. See https://www.apache.org/licenses/LICENSE-2.0
